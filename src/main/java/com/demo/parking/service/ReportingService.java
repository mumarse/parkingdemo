package com.demo.parking.service;

import java.util.List;

import com.demo.parking.entity.ParkingTicket;

public interface ReportingService {
	
	List<ParkingTicket> getParkingTicketsBetweenTwoDates(String type);
	List<ParkingTicket> getVechileParkingTicketsBetweenTwoDates(String type, String vechileNumber);
	
	//We can add more methods depends on requirements
}
