package com.demo.parking.service;

import com.demo.parking.entity.ParkingTicket;

import java.util.List;

public interface ParkingService {

    ParkingTicket entry(String vehicleNumber);
    ParkingTicket exit(Integer parkingTicketId);
    ParkingTicket findMissingTicket(String vehicleNumber);
    List<ParkingTicket> getAllParkingTickets();

    //We can add more methods depends on requirements

}
