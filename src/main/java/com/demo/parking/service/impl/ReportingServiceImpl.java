package com.demo.parking.service.impl;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.parking.dao.ParkingTicketDao;
import com.demo.parking.entity.ParkingTicket;
import com.demo.parking.service.ReportingService;

@Service
public class ReportingServiceImpl implements ReportingService{

	@Autowired private ParkingTicketDao parkingTicketDao;
	
	@Override
	public List<ParkingTicket> getParkingTicketsBetweenTwoDates(String type) {
		// TODO Auto-generated method stub
		List<ParkingTicket> ticketList = null;
		LocalDate now = LocalDate.now();
		  LocalDate weekStart = now;
		    while (weekStart.getDayOfWeek() != DayOfWeek.MONDAY)
		    {
		    	weekStart = weekStart.minusDays(1);
		    }
		    LocalDate weekhend = now;
		    while (weekhend.getDayOfWeek() != DayOfWeek.SUNDAY)
		    {
		    	weekhend = weekhend.plusDays(1);
		    }
		  YearMonth yearMonth = YearMonth.from(now);
		  
		  LocalDate monthStart = yearMonth.atDay(1);
		  LocalDate monthend   = yearMonth.atEndOfMonth();
		  
		  System.out.println("weekStart- "+weekStart+" weekhend- "+weekhend+" monthStart- "+monthStart+" monthend- "+monthend);

		if(type.equalsIgnoreCase("daily")) {
		}
		else if(type.equalsIgnoreCase("weekly")) {
		}
		else if(type.equalsIgnoreCase("monthly")) {
		}
		else {ticketList = parkingTicketDao.findAll();}
		return ticketList;
	}

	@Override
	public List<ParkingTicket> getVechileParkingTicketsBetweenTwoDates(String type,
			String vehicleNumber) {
		// TODO Auto-generated method stub
		List<ParkingTicket> ticketList = null;
		if(type.equalsIgnoreCase("daily")) {}
		else if(type.equalsIgnoreCase("weekly")) {}
		else if(type.equalsIgnoreCase("monthly")) {}
		else {ticketList = parkingTicketDao.findAllByVehicleNumberAndExitTimeNotNull(vehicleNumber);}
		return ticketList;
	}

}
