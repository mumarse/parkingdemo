package com.demo.parking.service.impl;

import com.demo.parking.dao.ParkingPriceDao;
import com.demo.parking.dao.ParkingTicketDao;
import com.demo.parking.entity.ParkingPrice;
import com.demo.parking.entity.ParkingTicket;
import com.demo.parking.service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class ParkingServiceImpl implements ParkingService {

    @Autowired private ParkingTicketDao parkingTicketDao;
    @Autowired private ParkingPriceDao parkingPriceDao;

    @Override
    public ParkingTicket entry(String vehicleNumber) {
        ParkingTicket parkingTicket = new ParkingTicket();
        parkingTicket.setVehicleNumber(vehicleNumber);
        /*Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, -26);
        parkingTicket.setEntryTime(calendar.getTime());*/
        parkingTicket.setEntryTime(new Date());
        return parkingTicketDao.save(parkingTicket);
    }

    @Override
    public ParkingTicket exit(Integer parkingTicketId) {
        List<ParkingPrice> parkingPrices = parkingPriceDao.findAll();
        Date currentTime = new Date();
        ParkingTicket parkingTicket = parkingTicketDao.findById(parkingTicketId).orElse(null);
        if(parkingTicket == null) throw new RuntimeException("Invalid Parking ticket");
        if(parkingTicket.getExitTime() != null) throw new RuntimeException("Invalid Parking ticket");

        //Calculate parking time in hours
        float parkingTime = (currentTime.getTime() - parkingTicket.getEntryTime().getTime())/(1000*60*60);
        boolean isWeekEnd = false;
        Calendar c1 = Calendar.getInstance();
        c1.setTime(currentTime);
        if ((c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) || (Calendar.DAY_OF_WEEK == Calendar.SUNDAY)) isWeekEnd = true;

        ParkingPrice appliedParkingPrice = null;
        float testParkingTime = parkingTime;
        if(testParkingTime > 24) testParkingTime = 24;
        for(ParkingPrice parkingPrice : parkingPrices){
            if(parkingPrice.getDurationInMaxHours() >= testParkingTime){
                appliedParkingPrice = parkingPrice;
                break;
            }
        }
        if(appliedParkingPrice == null) throw new RuntimeException("INTERNAL SERVER ERROR");

        parkingTicket.setExitTime(currentTime);

        if(parkingTime > 24){
            //TODO: Need to discuses
            double parkingDays = Math.ceil(parkingTime/24);
            //Calculate on Regular price
            parkingTicket.setBillingAmount(parkingDays * appliedParkingPrice.getRegularPrice());
        }else{
            parkingTicket.setBillingAmount(isWeekEnd ? appliedParkingPrice.getWeekendPrice() : appliedParkingPrice.getRegularPrice());
        }
        return parkingTicketDao.save(parkingTicket);
    }

    @Override
    public ParkingTicket findMissingTicket(String vehicleNumber) {
        return parkingTicketDao.findByVehicleNumberAndExitTimeNotNull(vehicleNumber);
    }

    @Override
    public List<ParkingTicket> getAllParkingTickets() {
        return parkingTicketDao.findAll();
    }
}
