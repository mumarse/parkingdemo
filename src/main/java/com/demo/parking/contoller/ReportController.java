package com.demo.parking.contoller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.parking.entity.ParkingTicket;
import com.demo.parking.service.ReportingService;

@RestController
public class ReportController {
	private static final Logger log = LogManager.getLogger(ReportController.class);
	
	@Autowired
    private ReportingService reportingService;

    @GetMapping("/report/{type}")
    public ResponseEntity<List<ParkingTicket>> allParkingTicketsBetweenTwoDates(@PathVariable String type){
        return ResponseEntity.ok(reportingService.getParkingTicketsBetweenTwoDates(type));
    }
    
    @GetMapping("/report/vehicle/type")
    public ResponseEntity<List<ParkingTicket>> vehicleAllParkingTicketsBetweenTwoDates(@RequestParam("type") String type, 
    		@RequestParam("vehicleNumber") String vehicleNumber){
        return ResponseEntity.ok(reportingService.getVechileParkingTicketsBetweenTwoDates(type, vehicleNumber));
    }
}
