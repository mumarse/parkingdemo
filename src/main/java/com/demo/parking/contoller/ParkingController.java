package com.demo.parking.contoller;

import com.demo.parking.entity.ParkingTicket;
import com.demo.parking.service.ParkingService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ParkingController {

    @Autowired
    private ParkingService parkingService;
    
    private static final Logger log = LogManager.getLogger(ParkingController.class);

    @GetMapping("/tickets")
    public ResponseEntity<List<ParkingTicket>> getTickets(){
    	log.info(" tickets url Successfully invoke ");
        return ResponseEntity.ok(parkingService.getAllParkingTickets());
    }

    @GetMapping("/ticket/entry/{vehicleNumber}")
    public ResponseEntity<ParkingTicket> entryInParking(@PathVariable String vehicleNumber){
    	log.info(" tickets/entry url Successfully invoke ");
        return ResponseEntity.ok(parkingService.entry(vehicleNumber));
    }

    @PutMapping("/ticket/exit/{ticketId}")
    public ResponseEntity<ParkingTicket> exitParking(@PathVariable Integer ticketId){
    	log.info(" tickets/exit url Successfully invoke ");
        return ResponseEntity.ok(parkingService.exit(ticketId));
    }

}
