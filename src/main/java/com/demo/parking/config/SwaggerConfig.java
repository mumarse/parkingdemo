package com.demo.parking.config;

import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.validation.Validator;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {

@Bean
public Docket productApi() {
return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.demo")).paths(PathSelectors.any()).build()
.apiInfo(metadata());
}

private ApiInfo metadata() {
return new ApiInfoBuilder().title("API Documentation").description("Description")
.version("1.0.0").license("Apache License Version 2.0").licenseUrl("https://www.apache.org/licenses/LICENSE-2.0\"")
.contact(new Contact("Umar", "domo.com/", "abc@gmail.com")).build();
}

@Override
public void addResourceHandlers(ResourceHandlerRegistry registry) {
registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
}

@Override
public void configurePathMatch(PathMatchConfigurer configurer) {

}

@Override
public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {

}

@Override
public void configureAsyncSupport(AsyncSupportConfigurer configurer) {

}

@Override
public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {

}

@Override
public void addFormatters(FormatterRegistry registry) {

}

@Override
public void addInterceptors(InterceptorRegistry registry) {

}

@Override
public void addCorsMappings(CorsRegistry registry) {

}

@Override
public void addViewControllers(ViewControllerRegistry registry) {

}

@Override
public void configureViewResolvers(ViewResolverRegistry registry) {

}

@Override
public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {

}

@Override
public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> returnValueHandlers) {

}

@Override
public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {

}

@Override
public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {

}

@Override
public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {

}

@Override
public void extendHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {

}

@Override
public Validator getValidator() {
return null;
}

@Override
public MessageCodesResolver getMessageCodesResolver() {
return null;
}

}