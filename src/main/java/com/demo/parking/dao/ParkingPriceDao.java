package com.demo.parking.dao;

import com.demo.parking.entity.ParkingPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingPriceDao  extends JpaRepository<ParkingPrice, Integer> {
}
