package com.demo.parking.dao;

import com.demo.parking.entity.ParkingTicket;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingTicketDao extends JpaRepository<ParkingTicket, Integer> {
    ParkingTicket findByVehicleNumberAndExitTimeNotNull(String vehicleNumber);

	List<ParkingTicket> findAllByVehicleNumberAndExitTimeNotNull(String vehicleNumber);
	List<ParkingTicket> findAll();
	@Query(value = "SELECT * FROM ParkingTicket WHERE entryTime = ?", nativeQuery = true)
	List<ParkingTicket> findTodayAllTicket();
}
