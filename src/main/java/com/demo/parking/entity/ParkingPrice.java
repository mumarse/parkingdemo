package com.demo.parking.entity;


import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity 
public class ParkingPrice implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ParkingPrice() {}
	
    public ParkingPrice(Integer id, Double durationInMaxHours, Double weekendPrice, Double regularPrice,
			String currencyCode) {
		super();
		this.id = id;
		this.durationInMaxHours = durationInMaxHours;
		this.weekendPrice = weekendPrice;
		this.regularPrice = regularPrice;
		this.currencyCode = currencyCode;
	}
	@Id
    private Integer id;
    private Double durationInMaxHours;
    private Double weekendPrice; //Price in Sat, Sun
    private Double regularPrice; //Price in Mon, Tues, Wed, Thus, Fri
    private String currencyCode;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getDurationInMaxHours() {
		return durationInMaxHours;
	}
	public void setDurationInMaxHours(Double durationInMaxHours) {
		this.durationInMaxHours = durationInMaxHours;
	}
	public Double getWeekendPrice() {
		return weekendPrice;
	}
	public void setWeekendPrice(Double weekendPrice) {
		this.weekendPrice = weekendPrice;
	}
	public Double getRegularPrice() {
		return regularPrice;
	}
	public void setRegularPrice(Double regularPrice) {
		this.regularPrice = regularPrice;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
    
    

}
