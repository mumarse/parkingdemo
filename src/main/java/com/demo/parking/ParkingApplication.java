package com.demo.parking;

import com.demo.parking.dao.ParkingPriceDao;
import com.demo.parking.entity.ParkingPrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import javax.annotation.PostConstruct;
import java.util.Arrays;

@SpringBootApplication
public class ParkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParkingApplication.class, args);
	}

	@Autowired
	private ParkingPriceDao parkingPriceDao;

	@PostConstruct
	public void init(){
		System.out.println("Initializing application");
		parkingPriceDao.saveAll(Arrays.asList(new ParkingPrice(1, 2d, 5d, 7d, "USD" ),
				new ParkingPrice(2, 5d, 8d, 10d, "USD" ),
				new ParkingPrice(3, 10d, 12d, 15d, "USD" ),
				new ParkingPrice(4, 15d, 18d, 22d, "USD" ),
				new ParkingPrice(5, 24d, 25d, 30d, "USD" )));

		System.out.println(parkingPriceDao.findAll());
	}

}
